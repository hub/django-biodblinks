BioDBLinks
==========

BioDBLinks is a Django app to help connect a bio-related django-based database
with other existing references databases.


Quick start
-----------

1. Add "polls" to your INSTALLED_APPS setting like this:

```python
INSTALLED_APPS = [
    ...
    'biodblinks',
]
```

2. Add the various configuration variables to the settings:

```python
# Django BioDbLinks settings
LABLINKS_ID_PREFIX = "https://biodbexample.org"
LABLINKS_PROVIDER_ID = "XXXX"
LABLINKS_RESOURCE_NAME = "BioDB"
LABLINKS_RESOURCE_DESCRIPTION = "BioDB : An example database"
LABLINKS_CONTACT_EMAIL = "curation@biodbexample.org"
```

3. Create a LabsLink export command for your data, in the `management/commands`
   folder of the relevant application. The only thing you need to declare is a 
   subclass of LabsLinkCommand with a queryset class property that generates 
   for each link the following fields, to be used in the _links_ file:
   - title
   - url
   - record_id and record_source
   - or doi
   An example command is available in the `biodblinks/tests/test_lablinks.py` 
   file.


