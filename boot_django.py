# boot_django.py

#

# This file sets up and configures Django. It's used by scripts that need to

# execute as if running in a Django server.

import os

import django

from django.conf import settings


BASE_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), "biodblinks"))


def boot_django():

    settings.configure(
        BASE_DIR=BASE_DIR,
        DEBUG=True,
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
            }
        },
        INSTALLED_APPS=("biodblinks",),
        TIME_ZONE="UTC",
        USE_TZ=True,
        LABLINKS_ID_PREFIX="https://django-biodblinks.pasteur.fr",
        LABLINKS_PROVIDER_ID="1111",
        LABLINKS_RESOURCE_NAME="django-biodblinks foo DB",
        LABLINKS_RESOURCE_DESCRIPTION="django-biodblinks foo DB: a non-existent database that should not be linked to",
        LABLINKS_CONTACT_EMAIL="noreply@pasteur.fr",
    )

    django.setup()
