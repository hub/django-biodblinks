# coding: utf-8

from django.conf import settings
from django.core.serializers.xml_serializer import Serializer as BaseSerializer
from django.core.serializers import base
from django.utils.xmlutils import (
    SimplerXMLGenerator,
    UnserializableContentError,
)


class Serializer(BaseSerializer):
    def start_serialization(self):
        """
        Start serialization -- open the XML document and the root element.
        """
        self.xml = SimplerXMLGenerator(
            self.stream, self.options.get("encoding", settings.DEFAULT_CHARSET)
        )
        self.xml.startDocument()
        self.xml.startElement("links", {})

    def end_serialization(self):
        """
        End serialization -- end the document.
        """
        self.indent(0)
        self.xml.endElement("links")
        self.xml.endDocument()

    def start_object(self, obj):
        """
        Called as each object is handled.
        """
        if not hasattr(obj, "_meta"):
            raise base.SerializationError(
                "Non-model object (%s) encountered during serialization" % type(obj)
            )

        self.indent(1)
        attrs = {"model": str(obj._meta)}
        if not self.use_natural_primary_keys or not hasattr(obj, "natural_key"):
            obj_pk = obj.pk
            if obj_pk is not None:
                attrs["pk"] = str(obj_pk)
        self.xml.startElement("link", {'providerId': settings.LABLINKS_PROVIDER_ID})
        self.xml.startElement("resource", {})
        self.indent(3)
        self.xml.startElement("title", {})
        self.xml.characters(obj.resource_title)
        self.xml.endElement("title")
        self.xml.startElement("url", {})
        self.xml.characters(obj.resource_url)
        self.xml.endElement("url")
        self.xml.endElement("resource")
        if obj.doi is not None:
            self.xml.startElement("doi", {})
            self.xml.characters(obj.doi)
            self.xml.endElement("doi")
        else:
            self.xml.startElement("record", {})
            self.indent(3)
            self.xml.startElement("source", {})
            self.xml.characters(obj.record_source)
            self.xml.endElement("source")
            self.xml.startElement("id", {})
            self.xml.characters(obj.record_id)
            self.xml.endElement("id")
            self.xml.endElement("record")

    def end_object(self, obj):
        """
        Called after handling all fields for an object.
        """
        self.indent(1)
        self.xml.endElement("link")

    def handle_field(self, obj, field):
        """
        Handle each field on an object (except for ForeignKeys and
        ManyToManyFields).
        """
        return

    def handle_fk_field(self, obj, field):
        """
        Handle a ForeignKey (they need to be treated slightly
        differently from regular fields).
        """
        return

    def _start_relational_field(self, field):
        """Output the <field> element for relational fields."""
        return
