# coding: utf-8
import xml.dom.minidom

from django.conf import settings
from django.core import serializers
from django.core.management import BaseCommand
from django.core.serializers import register_serializer

register_serializer("labslink-xml", "biodblinks.lablinks.serializer")


class LabsLinkCommand(BaseCommand):

    help = "Create the LabsLink files to be uploaded to PMC Europe"

    def add_arguments(self, parser):
        parser.add_argument(
            "profile_file", type=str, help="Path to the Profile file to be generated"
        )
        parser.add_argument(
            "links_file", type=str, help="Path to the Links file to be generated"
        )

    def generate_profile_file(self, profile_file_path):
        self.stdout.write(
            self.style.SUCCESS("Generating the Profile file in {profile_file_path}...".format(profile_file_path=profile_file_path))
        )
        lablinks_settings = {
            "provider_id": settings.LABLINKS_PROVIDER_ID,
            "resource_name":  settings.LABLINKS_RESOURCE_NAME,
            "resource_description": settings.LABLINKS_RESOURCE_DESCRIPTION,
            "contact_email": settings.LABLINKS_CONTACT_EMAIL,
        }
        xml_string = """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
        <providers>
            <provider>
                <id>{provider_id}</id>
                <resourceName>{resource_name}</resourceName>
                <description>{resource_description}</description>
                <email>{contact_email}</email>
            </provider>
        </providers>
        """.format(**lablinks_settings)
        xml_string = xml.dom.minidom.parseString(xml_string).toprettyxml()
        with open(profile_file_path, "w") as profile_file:
            profile_file.write(xml_string)
        self.stdout.write(
            self.style.SUCCESS(
                "Successfully generated the Profile file in {profile_file_path}...".format(profile_file_path=profile_file_path)
            )
        )

    def generate_links_file(self, links_file_path):
        self.stdout.write(
            self.style.SUCCESS("Generating the Links file in {links_file_path}...".format(links_file_path=links_file_path))
        )
        XMLSerializer = serializers.get_serializer("labslink-xml")
        xml_serializer = XMLSerializer()
        xml_serializer.serialize(self.queryset)
        data = xml_serializer.getvalue()
        xml_string = xml.dom.minidom.parseString(data).toprettyxml()
        with open(links_file_path, "w") as links_file:
            links_file.write(xml_string)
        self.stdout.write(
            self.style.SUCCESS(
                "Successfully generated the Links file in {links_file_path}...".format(links_file_path=links_file_path)
            )
        )

    def handle(self, *args, **options):
        self.generate_profile_file(options["profile_file"])
        self.generate_links_file(options["links_file"])
