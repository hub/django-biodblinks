from django.db import models


class Resource(models.Model):
    title = models.TextField("Resource Title")
    url = models.TextField("Resource URL")
    record_id = models.TextField("Record ID")
    record_source = models.TextField("Record source")
