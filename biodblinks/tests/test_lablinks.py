import tempfile

from django.core.management import call_command
from django.db.models import F, Value as V, TextField
from django.test import TestCase

from biodblinks.lablinks.command import LabsLinkCommand
from biodblinks.models import Resource


class TestCommand(LabsLinkCommand):

    queryset = (
        Resource.objects.annotate(resource_title=F("title"))
        .annotate(resource_url=F("url"))
        .annotate(doi=V("", output_field=TextField()))
    )


test_command = TestCommand()


class TestRunLabsLinkCommand(TestCase):
    def setUp(self):
        r = Resource()
        r.title = "test title"
        r.url = "http://example.com"
        r.record_id = 1
        r.record_source = "MED"
        r.save()

    def test_run_cmd(self):
        profile_file = tempfile.NamedTemporaryFile()
        links_file = tempfile.NamedTemporaryFile()
        call_command(test_command, profile_file.name, links_file.name)
