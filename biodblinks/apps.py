from django.apps import AppConfig


class BiodblinksConfig(AppConfig):
    name = "biodblinks"
